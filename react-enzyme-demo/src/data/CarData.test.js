import CarData from './CarData';

jest.mock('./CarData');

describe('CarData Mocking Demo', () => {

  test('Get All Cars', () => {

    const carData = new CarData();

    return carData.all().then(cars => expect(cars.length).toEqual(2));
  });

});