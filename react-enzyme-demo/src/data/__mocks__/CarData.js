export const mockAll = jest.fn(() => {

  return Promise.resolve([
    { id: 1, make: 'Test Make 1', model: 'Test Model 1', color: 'red', size: 'small', price: 1000 },
    { id: 2, make: 'Test Make 2', model: 'Test Model 2', color: 'blue', size: 'medium', price: 2000 },
  ]);

});

const mock = jest.fn().mockImplementation(() => {
  return { all: mockAll };
});

export default mock;