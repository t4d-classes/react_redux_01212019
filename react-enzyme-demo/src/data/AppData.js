import CarData from './CarData';

export class AppData {

  allCars() {
    return new CarData().all();
  }

}

export default AppData; 