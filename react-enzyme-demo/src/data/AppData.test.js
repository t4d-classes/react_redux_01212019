import AppData from './AppData';

jest.mock('./CarData');

describe('AppData Mocking Demo', () => {

  test('Get All Cars', () => {

    const appData = new AppData();

    return appData.allCars().then(cars => expect(cars.length).toEqual(2));
  });

});