Exercise 7

1. Move Car Form to its own component.

2. Utilize Car Form component in Car Tool.

3. For Car Form, specify a default prop for the button text.

4. Add PropTypes to Car Form, ensure the button text and onSubmitCar functions are passed.

Bonus: Add PropTypes to ToolHeader component. Ensure the headerText is passed.

5. Run the application, ensure it works.