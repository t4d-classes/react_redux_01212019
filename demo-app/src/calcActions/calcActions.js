export const CALC_ADD_ACTION = '[Calc] Add';
export const CALC_SUBTRACT_ACTION = '[Calc] Subtract';
export const CALC_MULTIPLY_ACTION = '[Calc] Multiply';
export const CALC_DIVIDE_ACTION = '[Calc] Divide';

export const createAddAction = value => ({ type: CALC_ADD_ACTION, payload: value });
export const createSubtractAction = value => ({ type: CALC_SUBTRACT_ACTION, payload: value });
export const createMultiplyAction = value => ({ type: CALC_MULTIPLY_ACTION, payload: value });
export const createDivideAction = value => ({ type: CALC_DIVIDE_ACTION, payload: value });
