export const change = componentInstance => e =>
  componentInstance.setState({
    [ e.target.name ]: e.target.type === 'number'
      ? Number(e.target.value)
      : e.target.value,
  });

export const textChange = componentInstance => e =>
  componentInstance.setState({
    [ e.target.name ]: e.target.value,
  });

export const numberChange = componentInstance => e =>
  componentInstance.setState({
    [ e.target.name ]: Number(e.target.value),
  });