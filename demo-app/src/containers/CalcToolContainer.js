import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  createAddAction, createDivideAction,
  createMultiplyAction, createSubtractAction
} from '../calcActions/calcActions';

import { CalcTool } from '../components/CalcTool';

export const CalcToolContainer = connect(
  ({ result, history }) => ({ result, history }),
  dispatch => bindActionCreators({
    onAdd: createAddAction,
    onSubtract: createSubtractAction,
    onMultiply: createMultiplyAction,
    onDivide: createDivideAction,
  }, dispatch),
)(CalcTool);

// const mapStateToProps = ({ result, history }) => ({ result, history });

// const mapDispatchToProps = dispatch => bindActionCreators({
//   onAdd: createAddAction,
//   onSubtract: createSubtractAction,
//   onMultiply: createMultiplyAction,
//   onDivide: createDivideAction,
// }, dispatch);

// const createCalcToolContainer = connect(
//   // state => { return { result: state.result, history: state.history }; },
//   // destructuring         short-hand properties
//   mapStateToProps,
//   mapDispatchToProps,
// );

// export const CalcToolContainer = createCalcToolContainer(CalcTool);
