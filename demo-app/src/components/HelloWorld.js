import React from 'react';

export const HelloWorld = () => {

  return <>
    <h1>Hello World!</h1>
    <br />
  </>;

  // return <React.Fragment>
  //   <h1>Hello World!</h1>
  //   <br />
  // </React.Fragment>;

  // return React.createElement('div', null,
  //   React.createElement('h1', null, 'Hello World!!!!'),
  //   React.createElement('br')
  // );

  // return React.createElement('h1', null, 'Hello World!!!!')React.createElement('br');

};
