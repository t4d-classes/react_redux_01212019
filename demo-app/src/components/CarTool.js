import React from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export class CarTool extends React.Component {

  componentDidMount() {
    this.props.onRefreshCars();
  }

  render() {
    return <>
      <ToolHeader headerText="Car Tool" />
      <CarTable cars={this.props.cars} editCarId={this.props.editCarId}
        onEditCar={this.props.onEdit} onDeleteCar={this.props.onDelete}
        onSaveCar={this.props.onReplace} onCancelCar={this.props.onCancel} />
      <CarForm buttonText="Add Car" onSubmitCar={this.props.onAppend} />
    </>;

  }
  
  

}