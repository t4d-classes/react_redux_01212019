import React, { useState } from 'react';
import PropTypes from 'prop-types';
 
import { textChange } from '../utils';

export const ColourFormWithStateHook = ({ onSubmitColour, buttonText }) => {

  // array destructuring
  // const colourState = useState('');
  const [ colour, setColour ] = useState({
    name: '',
    hexCode: '',
  });

  const submitColour = () => {
    onSubmitColour(colour);
    setColour({
      name: '',
      hexCode: '',
    });
  };

  return <form>
    <div>
      <label htmlFor="name-input">Name:</label>
      <input type="text" id="name-input"
        value={colour.name}
        onChange={e => setColour({
          ...colour, name: e.target.value
        })} name="colour" />
    </div>
    <div>
      <label htmlFor="hexcode-input">HexCode:</label>
      <input type="text" id="hexcode-input"
        value={colour.hexCode}
        onChange={e => setColour({
          ...colour, hexCode: e.target.value
        })} name="colour" />
    </div>
    <button type="button" onClick={submitColour}>
      {buttonText}
    </button>
  </form>;

};

ColourForm.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onSubmitColour: PropTypes.func.isRequired,
};

ColourForm.defaultProps = {
  buttonText: 'Submit Colour',
};