import React from 'react';
import PropTypes from 'prop-types';
 
import { textChange } from '../utils';

export class ColourFormWithClass extends React.Component {

  static propTypes = {
    buttonText: PropTypes.string.isRequired,
    onSubmitColour: PropTypes.func.isRequired,
  };

  static defaultProps = {
    buttonText: 'Submit Colour',
  };

  state = {
    colour: '',
  };

  // event handler so use an arrow function
  submitColour = () => {
    this.props.onSubmitColour(this.state.colour);
  };

  render() {
    return <form>
      <div>
        <label htmlFor="colour-input">Colour:</label>
        <input type="text" id="colour-input"
          value={this.state.colour}
          onChange={textChange(this)} name="colour" />
      </div>
      <button type="button" onClick={this.submitColour}>
        {this.props.buttonText}
      </button>
    </form>;
  }

}