import {
  CALC_ADD_ACTION, CALC_DIVIDE_ACTION,
  CALC_MULTIPLY_ACTION, CALC_SUBTRACT_ACTION
} from '../calcActions/calcActions';

export const historyReducer = (state = [], { type, payload }) => {

  switch (type) {
    case CALC_ADD_ACTION:
    case CALC_SUBTRACT_ACTION:
    case CALC_MULTIPLY_ACTION:
    case CALC_DIVIDE_ACTION:
      return state.concat(`${type} ${payload}`)
    default:
      return state;
  }

};