export const REFRESH_CARS_REQUEST = '[Cars] Refresh Cars Request';
export const REFRESH_CARS_DONE = '[Cars] Refresh Cars Done';

export const createRefreshCarsRequestAction = () => ({ type: REFRESH_CARS_REQUEST });
export const createRefreshCarsDoneAction = cars => ({ type: REFRESH_CARS_DONE, payload: cars });

export const refreshCars = () => {

  return async (dispatch, getState) => {

    dispatch(createRefreshCarsRequestAction());

    const res = await fetch('http://localhost:3050/cars');
    const cars = await res.json();

    dispatch(createRefreshCarsDoneAction(cars));
  };


};