import { refreshCars } from './refreshCars';

export const APPEND_CAR_REQUEST = '[Cars] Append Car Request';
export const APPEND_CAR_DONE = '[Cars] Append Car Done';

export const createAppendCarRequestAction = car => ({ type: APPEND_CAR_REQUEST, payload: car });
export const createAppendCarDoneAction = car => ({ type: APPEND_CAR_DONE, payload: car });

export const appendCar = car => {

  return async (dispatch, getState) => {

    dispatch(createAppendCarRequestAction(car));

    const res = await fetch('http://localhost:3050/cars', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    });
    const addedCar = await res.json();

    dispatch(createAppendCarDoneAction(addedCar));

    dispatch(refreshCars());
  };


};