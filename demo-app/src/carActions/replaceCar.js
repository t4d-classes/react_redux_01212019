import { refreshCars } from './refreshCars';

export const REPLACE_CAR_REQUEST = '[Cars] Replace Car Request';
export const REPLACE_CAR_DONE = '[Cars] Replace Car Done';

export const createReplaceCarRequestAction = car => ({ type: REPLACE_CAR_REQUEST, payload: car });
export const createReplaceCarDoneAction = car => ({ type: REPLACE_CAR_DONE, payload: car });

export const replaceCar = car => {

  return async (dispatch, getState) => {

    dispatch(createReplaceCarRequestAction(car));

    const res = await fetch('http://localhost:3050/cars/' + encodeURIComponent(car.id));
    const oldCar = await res.json();

    await fetch('http://localhost:3050/cars/' + encodeURIComponent(car.id), {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(car),
    });


    dispatch(createReplaceCarDoneAction(oldCar));

    dispatch(refreshCars());
  };


};