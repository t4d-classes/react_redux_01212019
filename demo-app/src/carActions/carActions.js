export const CAR_APPEND_ACTION = '[Car] Append';
export const CAR_REPLACE_ACTION = '[Car] Replace';
export const CAR_DELETE_ACTION = '[Car] Delete';
export const CAR_EDIT_ACTION = '[Car] Edit';
export const CAR_CANCEL_ACTION = '[Car] Cancel';

export const createCarAppendAction = payload => ({ type: CAR_APPEND_ACTION, payload });
export const createCarReplaceAction = payload => ({ type: CAR_REPLACE_ACTION, payload });
export const createCarDeleteAction = payload => ({ type: CAR_DELETE_ACTION, payload });
export const createCarEditAction = payload => ({ type: CAR_EDIT_ACTION, payload });
export const createCarCancelAction = () => ({ type: CAR_CANCEL_ACTION });