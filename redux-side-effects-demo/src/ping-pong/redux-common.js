export const PING = 'PING';
export const PONG = 'PONG';

export const ping = () => ({ type: PING });
export const pong = () => ({ type: PONG });

export const pingReducer = (state = { isPinging: false }, action) => {
  switch (action.type) {
    case PING:
      return { isPinging: true };
    case PONG:
      return { isPinging: false };
    default:
      return state;
  }
};