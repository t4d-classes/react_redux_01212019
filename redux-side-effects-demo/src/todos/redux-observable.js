import { createStore, applyMiddleware, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createEpicMiddleware, ofType, combineEpics } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import { mergeMap, map, switchMap } from 'rxjs/operators';

import { actionTypes, todosReducer } from './redux-common';
import { renderToDos, } from './components';


const refreshToDosEpic = action$ => action$.pipe(
  ofType(actionTypes.REFRESH_TODOS_REQUEST),
  switchMap(() => ajax({ url: 'http://localhost:3040/todos' })),
  map(({ response: todos }) => ({ type: actionTypes.REFRESH_TODOS_FULFILLED, todos })),
);

const addToDoEpic = action$ => action$.pipe(
  ofType(actionTypes.ADD_TODO_REQUEST),
  mergeMap(({ todo }) => ajax({
    url: 'http://localhost:3040/todos',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(todo),
  })),
  map(() => ({ type: actionTypes.REFRESH_TODOS_REQUEST }))
);

const removeToDoEpic = action$ => action$.pipe(
  ofType(actionTypes.REMOVE_TODO_REQUEST),
  mergeMap(({ todoId }) => ajax({
    url: 'http://localhost:3040/todos/' + encodeURIComponent(todoId),
    method: 'DELETE',
  })),
  map(() => ({ type: actionTypes.REFRESH_TODOS_REQUEST }))
);    

const toggleToDoCompletedEpic = action$ => action$.pipe(
  ofType(actionTypes.TOGGLE_TODO_COMPLETED_REQUEST),
  mergeMap(({ todo }) => ajax({
    url: 'http://localhost:3040/todos/' + encodeURIComponent(todo.id),
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ ...todo, completed: !todo.completed }),        
  })),
  map(() => ({ type: actionTypes.REFRESH_TODOS_REQUEST })),
);    

export const connectToDos = connect(
  ({ todos }) => ({ todos }),
  dispatch => bindActionCreators({
    refreshToDos: () => ({ type: actionTypes.REFRESH_TODOS_REQUEST }),
    addToDo: todo => ({ type: actionTypes.ADD_TODO_REQUEST, todo }),
    removeToDo: todoId => ({ type: actionTypes.REMOVE_TODO_REQUEST, todoId }),
    toggleToDoCompleted: todo => ({ type: actionTypes.TOGGLE_TODO_COMPLETED_REQUEST, todo }),
  }, dispatch)
);

const rootEpic = combineEpics(
  refreshToDosEpic,
  addToDoEpic,
  removeToDoEpic,
  toggleToDoCompletedEpic,
);

const epicMiddleware = createEpicMiddleware();

const store = createStore(todosReducer, applyMiddleware(epicMiddleware));

epicMiddleware.run(rootEpic);

renderToDos(connectToDos, store);

